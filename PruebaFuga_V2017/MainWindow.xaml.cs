﻿using PruebaFuga_V2017.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;




using com.jv.tools.util;

using PruebaFuga_V2017.Model;
using System.Timers;
using System.Windows.Threading;

namespace PruebaFuga_V2017
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        #region < LOGGER >

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        private Timer clock;
        private LeakTester tester;
        private KeyenceReader _reader;

        public MainWindow()
        {

            InitializeComponent();



            #region < CLOCK IMPLEMENTATION >
                clock = new Timer();
                clock.Interval = 1000;
                clock.Elapsed += Clock_Elapsed;
                clock.Start();
            #endregion
            string tittle = com.jv.tools.util.IniFile.IniReadValue(com.jv.tools.util.IniFile._PATH, "GENERAL", "name");

            this.lbTitle.Content = com.jv.tools.util.IniFile.IniReadValue(com.jv.tools.util.IniFile._PATH, "GENERAL", "name");

            DAO.GetInstance().testFunction();

            // pageTransitionControl.ShowPage(new View.Marker.PruebaFugaWindow());

            tester = new LeakTester(
                ConfigurationManager.Instance["LEAK_TESTER_port"],
                int.Parse(ConfigurationManager.Instance["LEAK_TESTER_baud"]),
                int.Parse(ConfigurationManager.Instance["DAQ_speed"]),
                int.Parse(ConfigurationManager.Instance["DAQ_out_line"]),
                ConfigurationManager.Instance["DAQ_out_port"]
                );


            tester.onChangeState += Tester_onChangeState;

            tester.OnNewPiece += Tester_OnNewPiece;

            tester.OnNewActivePartNumber += Tester_OnNewActivePartNumber;

            tester.OnClosingContainer += Tester_OnClosingContainer;

            tester.OnRaiseError += Tester_OnRaiseError;

            this.dataGrid.ItemsSource = tester.pieces;

            if(ConfigurationManager.Instance["KEYENCE_READER_enable"].ToLower().Trim() == "true")
            {


                _reader = new KeyenceReader(
                    ConfigurationManager.Instance["KEYENCE_READER_port"],
                    int.Parse(ConfigurationManager.Instance["KEYENCE_READER_baud"]));

                _reader.CodeRead += _reader_CodeRead;

                _reader.start();

                txScanner.Focus();

            }
            else
            {
                txScanner.IsEnabled = false;
            }


        }

        private void _reader_CodeRead(object sender, KeyenceReaderEventArgs e)
        {
            tester.proccesScaennerRead(e.code.Substring(0, e.code.Length - 1));
        }

        private void Tester_OnRaiseError(string errorMessage)
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {
                View.CustomMessageBox.showMessage("ERROR", errorMessage, 2000);
            }));

        }

        private void Tester_OnClosingContainer()
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {

                try
                {

                    CustomMessageBox.showMessage("AVISO", "CERRANDO CONTENEDOR", 1000);


                    this.txPieceN.Text = string.Format("{0}/{1}", tester._piecesOnContainer, tester._containerSize);

                }
                catch (Exception er)
                {
                    logger.Error(er, "Error al actualizar tabla en main form");
                }
            }));

        }

       

        private void Tester_OnNewActivePartNumber()
        {

            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {

                try
                {

                    lock (tester._currentPartNumber)
                    {
                        this.txPart.Text = tester._currentPartNumber;
                        this.txContainerN.Text = tester._currentContainer;

                        this.txPieceN.Text = string.Format("{0}/{1}", tester._piecesOnContainer, tester._containerSize);

                    }
                }
                catch (Exception er)
                {
                    logger.Error(er, "Error al actualizar tabla en main form");
                }
            }));


            
        }

        private void Tester_OnNewPiece()
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {

                try
                {

                    lock (tester.pieces)
                    {
                        this.txPieceN.Text = string.Format("{0}/{1}", tester._piecesOnContainer, tester._containerSize);

                        if (tester.pieces.Count >= 100)
                        {
                            tester.pieces.RemoveRange(99, 1);
                        }
                        dataGrid.ItemsSource = tester.pieces;
                        CollectionViewSource.GetDefaultView(dataGrid.ItemsSource).Refresh();

                    }
                }
                catch (Exception er)
                {
                    logger.Error(er, "Error al actualizar tabla en main form");
                }
            }));
        }

        private void Tester_onChangeState(object sender, EventArgs e)
        {
            showStatusOfOperation();
        }








        #region < CLOCK IMPLEMENTATION >
        private void Clock_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {

                try
                {
                    this.lbHour.Content = DateTime.Now.ToLongTimeString();


                }
                catch (Exception er)
                {
                    logger.Error(er);
                }


            }));
        }
        #endregion

        private void TextBox_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            this.txScanner.Focus();
        }

        private void txScanner_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                // vamos a notificar
                Console.WriteLine("Notificando");

                tester.proccesScaennerRead(txScanner.Text);

                this.txScanner.Text = "";

            }



        }


        private void showStatusOfOperation()
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {
                try
                {
                    switch (this.tester.CurrentStatus)
                    {
                        case LeakTester.status.REGISTERING_PART:
                            this.lbStatus.Content = "Registrando pieza";
                            break;
                        case LeakTester.status.TRIGGER_PF:
                            this.lbStatus.Content = "Iniciando ciclo";
                            break;
                        case LeakTester.status.UNDEFINED:
                            this.lbStatus.Content = "Indefinido";
                            break;
                        case LeakTester.status.WAITING_FOR_PF:
                            this.lbStatus.Content = "Esperando Prueba";
                            break;
                        case LeakTester.status.WAITING_FOR_SCANNER:
                            this.lbStatus.Content = "Esperando escaner";
                            break;

                    }
                    
                }
                catch (Exception er)
                {
                    logger.Error(er);
                }



            }));
        }




        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            tester.init();
            try
            {
                tester.start();
            }catch 
             {
                CustomMessageBox.showMessage("Error", "no se pudo establecer comunicacion con la pf", 3000);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (tester._piecesOnContainer > 0)
            {


                int result = CustomMessageBox.show("ALERTA", "¿Esta seguro de cerrar el contenedor?", CustomMessageBox._WARNING);

                if (result == CustomMessageBox._YES)
                {

                    tester.closeCointainer();
                }
            }
            else
            {
                CustomMessageBox.showMessage("AVISO", "EL CONTENEDOR ESTA VACIO", 1000);

            }
        }

        private void txScanner_LostFocus(object sender, RoutedEventArgs e)
        {
            txScanner.Focus();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
        }
    }
}

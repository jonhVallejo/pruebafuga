﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Timers;
using System.Windows.Threading;

namespace PruebaFuga_V2017.View
{
    /// <summary>
    /// Lógica de interacción para CustomMessageBox.xaml
    /// </summary>
    public partial class CustomMessageBox : Window
    {

        #region < MESSAGE CONSTANTS >

        public const int _ERROR = 0;
        public const int _WARNING = 1;
        public const int _SUCCESS = 2;
        public const int _INFORMATION = 3;
        public const int _YES_NO_CANCEL = 7;
        public const int _YES = 4;
        public const int _NO = 5;
        public const int _CANCEL = 6;

        private Timer timerTimeOut;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        private static int result;

        /// <summary>
        /// Constructor privado, CustomMessageBox no puede ser instanciado, solo usuado como un message box.
        /// </summary>
        private CustomMessageBox()
        {
            result = 0;
            InitializeComponent();

        }

        /// <summary>
        /// Constructor privado, CustomMessageBox no puede ser instanciado, solo usuado como un message box.
        /// </summary>
        private CustomMessageBox(int timeOut)
        {
            result = 0;
            InitializeComponent();
            
            timerTimeOut = new Timer();
            timerTimeOut.Interval = timeOut;

            timerTimeOut.Elapsed += timerTimeOut_Tick;

            timerTimeOut.Start();
           
            



        }

        void closeTimeOut()
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {


                timerTimeOut.Stop();
                result = _SUCCESS;
                base.Close();
            }));
        }

        void timerTimeOut_Tick(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {

            
            timerTimeOut.Stop();
            result = _SUCCESS;
            base.Close();
        }));
        }


        private void selectCombinationColors(int messageType)
        {
            switch (messageType)
            {
                case _ERROR:
                    lbMessage.Background = Brushes.Red;
                    lbMessage.Foreground = Brushes.White;
                    break;
                case _WARNING:
                    lbMessage.Background = Brushes.Orange;
                    lbMessage.Foreground = Brushes.White;
                    break;
                case _SUCCESS:
                    break;
                case _INFORMATION:
                    break;
            }
        }

        /// <summary>
        /// Muestra un mensaje emerjente, ocupa un esqueleton de esta misma clase para mostrar el mensaje.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static int show(string title, string message, int type)
        {

            CustomMessageBox msg = new CustomMessageBox();
            msg.lbTitle.Content = title;

            msg.selectCombinationColors(type);



            msg.lbMessage.Text = message;
            msg.updateLayOut(message.Length);
            msg.ShowDialog();

            Console.WriteLine("_____________________________________________________");
            Console.WriteLine("GRID ALTURA: {0}", msg.rootGrid.ActualHeight);
            Console.WriteLine("GRID ANCHO: {0}", msg.rootGrid.ActualWidth);
            Console.WriteLine("_____________________________________________________");

            return result;
        }


        public static int show(String title, string message)
        {
            CustomMessageBox msg = new CustomMessageBox();
            msg.lbTitle.Content = title;

            msg.lbMessage.Text = message;
            msg.updateLayOut(message.Length);
            msg.ShowDialog();

            Console.WriteLine("_____________________________________________________");
            Console.WriteLine("GRID ALTURA: {0}", msg.rootGrid.ActualHeight);
            Console.WriteLine("GRID ANCHO: {0}", msg.rootGrid.ActualWidth);
            Console.WriteLine("_____________________________________________________");



            return result;
        }

        public static int show(string message)
        {
            CustomMessageBox msg = new CustomMessageBox();
            msg.lbTitle.Content = "";

            msg.lbMessage.Text = message;
            msg.updateLayOut(message.Length);
            msg.ShowDialog();

            return result;
        }

        public static void showMessage(String title, string message)
        {
            CustomMessageBox msg = new CustomMessageBox();

            msg.btCancel.Visibility = Visibility.Hidden;

            msg.btNok.Visibility = Visibility.Hidden;

            Grid.SetColumn(msg.btOk, 4);

            msg.rootGrid.Children.Remove(msg.btCancel);
            msg.rootGrid.Children.Remove(msg.btNok);
            Grid.SetColumn(msg.btOk, 4);

            msg.btOk.Content = "OK";

            msg.lbTitle.Content = title;

            msg.lbMessage.Text = message;
            msg.updateLayOut(message.Length);


            msg.ShowDialog();
        }

        public static void showMessage(String title, string message, int timeOut)
        {
            
            CustomMessageBox msg = new CustomMessageBox(timeOut);

            msg.btCancel.Visibility = Visibility.Hidden;

            msg.btNok.Visibility = Visibility.Hidden;

            Grid.SetColumn(msg.btOk, 4);
            msg.btCancel.Visibility = Visibility.Hidden;
            msg.btOk.Visibility = Visibility.Hidden;
            msg.btNok.Visibility = Visibility.Hidden;


            msg.rootGrid.Children.Remove(msg.btCancel);
            msg.rootGrid.Children.Remove(msg.btNok);
            Grid.SetColumn(msg.btOk, 4);

            msg.btOk.Content = "OK";

            msg.lbTitle.Content = title;

            msg.lbMessage.Text = message;
            msg.lbMessage.FontSize = 36;

            msg.updateLayOut(message.Length);


            msg.ShowDialog();
        }


        private void updateLayOut(int messageLength)
        {
            //Estimar el tamaño de pixeles utilizados.
            //Estimado de caracteres por linea: 50
            //Lineas por defecto: 3
            int numberOfCharsForLine = 50;
            int defaultNumberOfLines = 3;
            if (messageLength > numberOfCharsForLine * defaultNumberOfLines)
            {
                //Redimencionar el frame de acuerdo a lo que se necesite de mas en lineas
                int rest = messageLength - (numberOfCharsForLine * defaultNumberOfLines);
                int v = rest;
                rest /= numberOfCharsForLine * defaultNumberOfLines;

                // El resultado es el numero de lineas adicionales, que sera traducido en un aproximado de pixeles necesarios para ajustar al tamaño
                rest += v % numberOfCharsForLine * defaultNumberOfLines == 0 ? 0 : 1;
                rootGrid.Height += rest * 20 * 5;
                lbMessage.Height += rest * 20;
            }


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btOk_Click(object sender, RoutedEventArgs e)
        {
            result = _YES;
            base.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btNok_Click(object sender, RoutedEventArgs e)
        {
            result = _NO;
            base.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            result = _CANCEL;
            base.Close();
        }

        private void rootContainer_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Escape)
            {
                result = _YES;
                base.Close();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Timers;

namespace PruebaFuga_V2017.View
{
    /// <summary>
    /// Lógica de interacción para PasswordDlg.xaml
    /// </summary>
    public partial class PasswordDlg : Window
    {

        private Timer tm;

        private static PasswordDlg _singletonInstance;


        private static string password;

        private PasswordDlg()
        {
            InitializeComponent();

            this.txPassword.Focus();

            tm = new Timer();

            tm.Interval = 20000;

            tm.Elapsed += tm_Tick;
        }

        public static PasswordDlg GetInstance()
        {
            if (_singletonInstance == null)
            {
                _singletonInstance = new PasswordDlg();
            }

            return _singletonInstance;
        }

        public static string show()
        {
            PasswordDlg dlg = new PasswordDlg();

            dlg.ShowDialog();

            return password;
        }

        void tm_Tick(object sender, EventArgs e)
        {
            //   throw new NotImplementedException();
        }

        private void bt1_Click(object sender, RoutedEventArgs e)
        {
            this.txPassword.Password += "1";
        }

        private void bt2_Click(object sender, RoutedEventArgs e)
        {
            this.txPassword.Password += "2";

        }

        private void bt3_Click(object sender, RoutedEventArgs e)
        {
            this.txPassword.Password += "3";

        }

        private void bt4_Click(object sender, RoutedEventArgs e)
        {
            this.txPassword.Password += "4";

        }

        private void bt5_Click(object sender, RoutedEventArgs e)
        {
            this.txPassword.Password += "5";

        }

        private void bt6_Click(object sender, RoutedEventArgs e)
        {
            this.txPassword.Password += "6";

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.txPassword.Password += "7";

        }

        private void bt8_Click(object sender, RoutedEventArgs e)
        {
            this.txPassword.Password += "8";

        }

        private void bt9_Click(object sender, RoutedEventArgs e)
        {
            this.txPassword.Password += "9";

        }

        private void bt0_Click(object sender, RoutedEventArgs e)
        {
            this.txPassword.Password += "0";

        }

        private void btAccept_Click(object sender, RoutedEventArgs e)
        {
            password = this.txPassword.Password;

            if (isPasswordValid(password))
            {
                base.Close();
            }
            else
            {
                View.CustomMessageBox.showMessage("ERROR", "CONTRASEÑA INVALIDA", 1000);
            }


            // base.Close();
        }


        private bool isPasswordValid(string password)
        {
            string _passwordInConfigFile;

            try
            {

                _passwordInConfigFile = ConfigurationManager.Instance["SECURITY_password"];

                if (_passwordInConfigFile.Trim() == "")
                {
                    _passwordInConfigFile = "01012017";
                }
            }
            catch (Exception e)
            {
                _passwordInConfigFile = "01012017";
            }



            return password == _passwordInConfigFile;
        }

        private void Window_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            //if (e.Key == Key.Enter)
            //{
            // password = this.txPassword.Password;
            //base.Close();
            //}//
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using com.jv.tools.util;
using com.jv.daq.Hardware;


using PruebaFuga_V2017.Model;

using PruebaFuga_V2017.View.Splasher;



namespace PruebaFuga_V2017
{
    class ApplicationController
    {
        #region < LOGGER >

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        private static ApplicationController _singletonInstance;

        private ApplicationController()
        {

        }

        public static ApplicationController GetInstance()
        {
            if( _singletonInstance == null)
            {
                _singletonInstance = new ApplicationController();
            }

            return _singletonInstance;
        }

        
        public void SystemStartup()
        {

            
            MessageListener.Instance.ReceiveMessage("Inicializando aplicación...");

            #region < LOAD CONFIGURATION >

            System.Threading.Thread.Sleep(100);
            MessageListener.Instance.ReceiveMessage("Cargando configuración...");
            System.Threading.Thread.Sleep(100);

            #region < DATA BASE >

            try
            {
                MessageListener.Instance.ReceiveMessage("Cargando parametros de la base de datos...");

                System.Threading.Thread.Sleep(200);
                string _host, _port, _database, _password, _user;

                _host = IniFile.IniReadValue(IniFile._PATH, "DATA_BASE", "server");

                ConfigurationManager.Instance["DATA_BASE_server"] = _host;

                
                _port = IniFile.IniReadValue(IniFile._PATH, "DATA_BASE", "port");
                ConfigurationManager.Instance["DATA_BASE_port"] = _port;
                _database = IniFile.IniReadValue(IniFile._PATH, "DATA_BASE", "sid");
                ConfigurationManager.Instance["DATA_BASE_sid"] = _database;
                _password = IniFile.IniReadValue(IniFile._PATH, "DATA_BASE", "password");
                ConfigurationManager.Instance["DATA_BASE_password"] = _password;
                _user = IniFile.IniReadValue(IniFile._PATH, "DATA_BASE", "user");
                ConfigurationManager.Instance["DATA_BASE_user"] = _user;
                MessageListener.Instance.ReceiveMessage(string.Format("Conectando con base de datos {0}", _database));
                try
                {
                    DAO.GetInstance().init(_host, int.Parse(_port), _database, _password, _user);

                    MessageListener.Instance.ReceiveMessage("Conexión establecida...");
                }
                catch (Exception e)
                {
                    MessageListener.Instance.ReceiveMessage(e.Message);

                }


                System.Threading.Thread.Sleep(100);
            }
            catch (Exception er)
            {
                logger.Error(er, "Hay algun parametro faltante, favor de verificar. Inposible crear conexion");
                throw er;
            }


            #endregion

            #region < DAQ >


            MessageListener.Instance.ReceiveMessage("Cargando configuración de DAQ...");

            string ARDUNO_port = IniFile.IniReadValue(IniFile._PATH, "DAQ", "port");

            ConfigurationManager.Instance["DAQ_speed"] = IniFile.IniReadValue(IniFile._PATH, "DAQ", "speed");
            ConfigurationManager.Instance["DAQ_out_line"] = IniFile.IniReadValue(IniFile._PATH, "DAQ", "out_line");
            ConfigurationManager.Instance["DAQ_out_port"] = IniFile.IniReadValue(IniFile._PATH, "DAQ", "out_port");

            ConfigurationManager.Instance["DAQ_port"] = ARDUNO_port;



            ARDUNO_Device board;

            board = new ARDUNO_Device(ARDUNO_port);


            MessageListener.Instance.ReceiveMessage("Cargando información de la prueba de fuga");

            ConfigurationManager.Instance["LEAK_TESTER_port"] = IniFile.IniReadValue(IniFile._PATH, "LEAK_TESTER", "port");
            ConfigurationManager.Instance["LEAK_TESTER_baud"] = IniFile.IniReadValue(IniFile._PATH, "LEAK_TESTER", "baud");

            ConfigurationManager.Instance["LEAK_TESTER_regex"] = IniFile.IniReadValue(IniFile._PATH, "LEAK_TESTER", "regex");
            
            

            // board.Ports.

            IOService.GetInstance().putDevice(board);



            ConfigurationManager.Instance["SECURITY_password"] = IniFile.IniReadValue(IniFile._PATH, "SECURITY", "password");

            ConfigurationManager.Instance["GENERAL_estacion"] = IniFile.IniReadValue(IniFile._PATH, "GENERAL", "estacion");


            ConfigurationManager.Instance["GENERAL_pattern"] = IniFile.IniReadValue(IniFile._PATH, "GENERAL", "pattern");
            #endregion

            #region < KEYENCE READER >

            MessageListener.Instance.ReceiveMessage("Cargando información de lector KEYENCE");

            ConfigurationManager.Instance["KEYENCE_READER_enable"] = IniFile.IniReadValue(IniFile._PATH, "KEYENCE_READER", "enable");

            if(ConfigurationManager.Instance["KEYENCE_READER_enable"].ToLower().Trim() == "true")
            {
                ConfigurationManager.Instance["KEYENCE_READER_port"] = IniFile.IniReadValue(IniFile._PATH, "KEYENCE_READER", "port");
                ConfigurationManager.Instance["KEYENCE_READER_baud"] = IniFile.IniReadValue(IniFile._PATH, "KEYENCE_READER", "baud");

            }
            else
            {
                MessageListener.Instance.ReceiveMessage("El lector de codigo de barras esta deshabilitado.");
            }




            #endregion

            #endregion

        }
    }

    partial class ConfigurationManager : Dictionary<string, string>
    {
        private static ConfigurationManager _singletonInstance;

        private ConfigurationManager()
        {

        }

        public static ConfigurationManager Instance
        {
            get
            {


                if (_singletonInstance == null)
                {
                    _singletonInstance = new ConfigurationManager();
                }

                return _singletonInstance;
            }
        }

        //public string 
    }
}

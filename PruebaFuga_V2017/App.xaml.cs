﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

using PruebaFuga_V2017.View.Splasher;

namespace PruebaFuga_V2017
{
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Console.WriteLine("iniciando!!!");
            Splasher.Splash = new View.Splasher.SplashScreen();

            Splasher.ShowSplash();

            for (int i = 0; i < 1000; i++)
            {
              //  MessageListener.Instance.ReceiveMessage(string.Format("Load module {0}", i));
             //   System.Threading.Thread.Sleep(1);
            }

            ApplicationController.GetInstance().SystemStartup();

            Splasher.HideSplash();

        }
    }
}

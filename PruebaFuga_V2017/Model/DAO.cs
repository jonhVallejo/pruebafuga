﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Data;

namespace PruebaFuga_V2017.Model
{
    class DAO
    {

        #region < LOGGER >

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        private String stringConnection;
        private String user;
        private String pass;
        private String[] ret;

        private OracleConnection connection;
        private OracleCommand command;


        private static DAO _singletonInstance;

        int iTry;

      


        private DAO()
        {
            int[] v = new int[100];
        }

        internal static DAO GetInstance()
        {
            if (_singletonInstance == null)
            {
                _singletonInstance = new DAO();
            }
            return _singletonInstance;
        }

        internal void init(string host, int port, string dataBase, string password, string user)
        {
            this.user = user;
            this.pass = password;
           
            stringConnection = "Data Source=(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP) "
                    + "(HOST = " + host + ")(PORT = " + port + "))(CONNECT_DATA = (SERVER = DEDICATED)" +
                        "(SERVICE_NAME = " + dataBase + ")));Password=" + password + ";User ID=" + user;
            connection = new OracleConnection();
            connection.ConnectionString = stringConnection;

            if (!open())
            {
                throw new Exception("No se pudo establecer conexión con al base de datos");
            }
            else
            {
                close();
            }
        }

        internal void init()
        {
            if (stringConnection == null) throw new Exception("Primero debe cargar la configuración de la base de datos.");
            connection = new OracleConnection();
            connection.ConnectionString = stringConnection;

            if (!open())
            {
                throw new Exception("No se pudo establecer conexión con al base de datos");
            }
            else
            {
                close();
            }
        }

        private bool open()
        {
            bool result = true;
            try
            {
                connection.Open();
            }
            catch (OracleException oe) // Se intentara reconectar 3 veces, si no lo logra manda el error
            {
                if (iTry == 3)
                {
                    logger.Error(oe, "No se pudo conectar con la base de datos");
                    return false;
                }
                iTry++;
                init();
                result = open();
            }
            return result;
        }

        private void close()
        {
            try
            {
                connection.Close();
            }
            catch (OracleException oe)
            {
                logger.Error(oe, "no se pudo cerar la base de datos");
                return;
            }
        }


        public void closeContainer(ref string[] messages)
        {

        }


        public int numParteActual(string estacion, ref string[] messages)
        {
            if (!open()) throw new Exception("No hay conexion con la base de datos");

            int result = 0;

            string procedure = "SP_WAL_NUMPART_ACTUAL";

            command = new OracleCommand(procedure, connection);
            command.CommandType = CommandType.StoredProcedure;
            /*
             CREATE OR REPLACE PROCEDURE SP_WAL_NUMPART_ACTUAL(p_estacion in varchar2,
                                                  p_part out varchar2,
                                                  p_pzas out varchar2,
                                                  p_result out varchar2,
                                                  p_msg out varchar2 ) is
             * */

            command.Parameters.Add("p_estacion", OracleDbType.Varchar2, 24);
            command.Parameters["p_estacion"].Value = estacion;

            command.Parameters.Add("p_part", OracleDbType.Varchar2, 50);
            command.Parameters["p_part"].Direction = ParameterDirection.Output;

            command.Parameters.Add("p_pzas", OracleDbType.Varchar2, 50);
            command.Parameters["p_pzas"].Direction = ParameterDirection.Output;

            command.Parameters.Add("p_result", OracleDbType.Varchar2);
            command.Parameters["p_result"].Direction = ParameterDirection.Output;
            command.Parameters.Add("p_msg", OracleDbType.Varchar2, 256);
            command.Parameters["p_msg"].Direction = ParameterDirection.Output;


            try
            {
                command.ExecuteNonQuery();


                messages[0] = command.Parameters["p_part"].Value.ToString();
                messages[1] = command.Parameters["p_pzas"].Value.ToString();
                messages[2] = command.Parameters["p_result"].Value.ToString();
                messages[3] = command.Parameters["p_msg"].Value.ToString();


                int.TryParse(command.Parameters["p_result"].Value.ToString(), out result);

             
            }
            catch (Exception er)
            {
                messages[3] = "Sucedio un error mientras se ejecutaba el procedimiento SP_JC49_REGISTRA_BALA";
                logger.Error(er, messages[3]);
                result = -3;
            }
            finally
            {
                close();

            }
            return result;
        }

        public int validaEtiqueta(string code, string parteActual, ref string[] messages)
        {
            if (!open()) throw new Exception("No hay conexion con la base de datos");

            int result = 0;

            string procedure = "SP_WAL_VALIDA_ETIQUETA";

            command = new OracleCommand(procedure, connection);
            command.CommandType = CommandType.StoredProcedure;
            /*
             "CREATE OR REPLACE PROCEDURE SP_WAL_VALIDA_ETIQUETA (
                                                    p_parte_act in varchar2,
                                                    p_etiqueta in varchar2,
                                                    p_err out number, 
                                                    p_msg out varchar2) is
             * */

            command.Parameters.Add("p_parte_act", OracleDbType.Varchar2, 24);
            command.Parameters["p_parte_act"].Value = parteActual;
            command.Parameters.Add("p_etiqueta", OracleDbType.Varchar2, 24);
            command.Parameters["p_etiqueta"].Value = code;
            command.Parameters.Add("p_err", OracleDbType.Int32);
            command.Parameters["p_err"].Direction = ParameterDirection.Output;
            command.Parameters.Add("p_msg", OracleDbType.Varchar2, 256);
            command.Parameters["p_msg"].Direction = ParameterDirection.Output;


            try
            {
                command.ExecuteNonQuery();

                messages[0] = command.Parameters["p_msg"].Value.ToString();

                messages[1] = command.Parameters["p_err"].Value.ToString();





                int.TryParse(command.Parameters["p_err"].Value.ToString(), out result);

               

            }
            catch (Exception er)
            {
                messages[0] = "Sucedio un error mientras se ejecutaba el procedimiento SP_JC49_REGISTRA_BALA";
                logger.Error(er, messages[3]);
                result = -3;
            }
            finally
            {
                close();

            }
            return result;
        }

        public int registraPieza(string prueba, string sensor, string parte, string codeScanner, ref string[] messages)
        {
            if (!open()) throw new Exception("No hay conexion con la base de datos");

            int result = 0;
            string procedure = "SP_WAL_REG_RESULTADO_SENSORPF3";

            command = new OracleCommand(procedure, connection);
            command.CommandType = CommandType.StoredProcedure;
            /*
            create or replace PROCEDURE "SP_WAL_REG_RESULTADO_SENSORPF3" (p_prueba in varchar2,
                                                          p_sensor in varchar2,
                                                          p_parte in varchar2,
                                                          p_seq_esc in number,
                                                          
                                                          p_seq out number,
                                                          p_cod_reg out varchar2,
                                                          p_cod_part out varchar2,
                                                          p_seq_cont out number, --code contenedor
                                                          p_piezas out number,   --piezas del contenedor
                                                          p_piezas_prog out number,  --piezas programadas del contenedor
                                                          p_cerrado out varchar2, --Y si el contenedor está cerrado; N en otro caso
                                                          p_err out number,
                                                          p_msg out varchar2 ) is
             */
            command.Parameters.Add("p_prueba",   OracleDbType.Varchar2, 24);
            command.Parameters["p_prueba"].Value = prueba;
            command.Parameters.Add("p_sensor",   OracleDbType.Varchar2, 24);
            command.Parameters["p_sensor"].Value = sensor;
            command.Parameters.Add("p_parte", OracleDbType.Varchar2, 24);
            command.Parameters["p_parte"].Value  = parte;
            command.Parameters.Add("p_seq_esc", OracleDbType.Varchar2, 24);
            command.Parameters["p_seq_esc"].Value = codeScanner;


            command.Parameters.Add("p_seq", OracleDbType.Varchar2, 24);
            command.Parameters["p_seq"].Direction = ParameterDirection.Output;
            command.Parameters.Add("p_cod_reg", OracleDbType.Varchar2, 24);
            command.Parameters["p_cod_reg"].Direction = ParameterDirection.Output;
            command.Parameters.Add("p_cod_part", OracleDbType.Varchar2, 24);
            command.Parameters["p_cod_part"].Direction = ParameterDirection.Output;
            command.Parameters.Add("p_seq_cont", OracleDbType.Varchar2, 24);
            command.Parameters["p_seq_cont"].Direction = ParameterDirection.Output;
            command.Parameters.Add("p_piezas", OracleDbType.Varchar2, 24);
            command.Parameters["p_piezas"].Direction = ParameterDirection.Output;
            command.Parameters.Add("p_piezas_prog", OracleDbType.Varchar2, 24);
            command.Parameters["p_piezas_prog"].Direction = ParameterDirection.Output;
            command.Parameters.Add("p_cerrado", OracleDbType.Varchar2, 2);
            command.Parameters["p_cerrado"].Direction = ParameterDirection.Output;
            command.Parameters.Add("p_err", OracleDbType.Int32);
            command.Parameters["p_err"].Direction = ParameterDirection.Output;
            command.Parameters.Add("p_msg", OracleDbType.Varchar2, 256);
            command.Parameters["p_msg"].Direction = ParameterDirection.Output;
            try
            {
                command.ExecuteNonQuery();
                messages[0] = command.Parameters["p_seq"].Value.ToString();
                messages[1] = command.Parameters["p_cod_reg"].Value.ToString();
                messages[2] = command.Parameters["p_cod_part"].Value.ToString();
                messages[3] = command.Parameters["p_seq_cont"].Value.ToString();
                messages[4] = command.Parameters["p_piezas"].Value.ToString();
                messages[5] = command.Parameters["p_piezas_prog"].Value.ToString();
                messages[6] = command.Parameters["p_cerrado"].Value.ToString();
                messages[7] = command.Parameters["p_msg"].Value.ToString();

                int.TryParse(command.Parameters["p_err"].Value.ToString(), out result);

                
            }
            catch (Exception er)
            {
                messages[7] = "Sucedio un error mientras se ejecutaba el procedimiento SP_JC49_REGISTRA_BALA";
                logger.Error(er, messages[7]);
                result = -3;
            }
            finally
            {
                close();
            }
            return result;
        }

        public int registerBadPiece(string leakTesterResult, ref string[] messages)
        {
            if (!open()) throw new Exception("No hay conexion con la base de datos");

            int result = 0;
            string procedure = "sp_mez_registra_pieza_pf";
            /*
            create or replace PROCEDURE "SP_MEZ_REGISTRA_PIEZA_PF"(p_valor in varchar2,
                                                  p_prueba in varchar2,
                                                  p_res   out number,
                                                  p_msg   out varchar2) is     */
            command = new OracleCommand(procedure, connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("P_VALOR", OracleDbType.Varchar2, 2);
            command.Parameters["P_VALOR"].Value = "M";
            command.Parameters.Add("P_PRUEBA", OracleDbType.Varchar2, 24);
            command.Parameters["P_PRUEBA"].Value = leakTesterResult;
            command.Parameters.Add("p_res", OracleDbType.Int32);
            command.Parameters["p_res"].Direction = ParameterDirection.Output;
            command.Parameters.Add("p_msg", OracleDbType.Varchar2, 256);
            command.Parameters["p_msg"].Direction = ParameterDirection.Output;

            try
            {
                command.ExecuteNonQuery();
                messages[0] = command.Parameters["p_msg"].Value.ToString();
                int.TryParse(command.Parameters["p_res"].Value.ToString(), out result);
                messages[1] = string.Format("{0}", result);
            }
            catch (Exception er)
            {
                messages[2] = "Sucedio un error mientras se ejecutaba el procedimiento sp_mez_registra_pieza_pf";
                logger.Error(er, messages[2]);
                result = -3;
            }
            finally
            {
                close();
            }

            return result;
        }


        public void testFunction()
        {
            if (!open()) throw new Exception("No hay conexion con la base de datos");

            int result = 0;
            string[] messages = new string[3];
            string procedure = "sp_test_function";
            command = new OracleCommand(procedure, connection);
            command.CommandType = CommandType.StoredProcedure;
            
            command.Parameters.Add("p_res", OracleDbType.Int32);
            command.Parameters["p_res"].Direction = ParameterDirection.Output;
            command.Parameters.Add("p_msg", OracleDbType.Varchar2, 256);
            command.Parameters["p_msg"].Direction = ParameterDirection.Output;

            try
            {
                command.ExecuteNonQuery();
                messages[0] = command.Parameters["p_msg"].Value.ToString();
                int.TryParse(command.Parameters["p_res"].Value.ToString(), out result);
                messages[1] = string.Format("{0}", result);
            }
            catch (Exception er)
            {
                messages[2] = "Sucedio un error mientras se ejecutaba el procedimiento";
                logger.Error(er, messages[2]);
                result = -3;
            }
            finally
            {
                close();
            }

        }
    }

}

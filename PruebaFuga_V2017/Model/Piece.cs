﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaFuga_V2017.Model
{
    class Piece
    {
        public string part
        {
            get;
            set;
        }

        public DateTime date
        {
            get;
            set;
        }

        public string pfString
        {
            get;
            set;
        }

        public string pfResult
        {
            get;
            set;
        }

        public string sequence
        {
            get;
            set;
        }

        public string status
        {
            get;
            set;
        }
    }
}

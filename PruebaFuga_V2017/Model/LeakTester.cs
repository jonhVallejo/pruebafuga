﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO.Ports;
using System.Timers;


using com.jv.daq.Hardware;
using System.Text.RegularExpressions;

namespace PruebaFuga_V2017.Model
{
#region <DELEGATES>
    delegate void NewPiece();
    delegate void NewActivePartNumber();

    delegate void ClosingContainer();


    delegate void RaiseError(string errorMessage);
#endregion

    public class LeakTester
    {

        #region < LOGGER >

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        internal List<Piece> pieces;

        internal enum status
        {
            UNDEFINED,
            WAITING_FOR_SCANNER,
            TRIGGER_PF,
            WAITING_FOR_PF,
            REGISTERING_PART
        }

        internal status CurrentStatus
        {
            get;
            set;
        }

        private string _leakTesterPort;
        private int _leakTesterBaudRate;

        private SerialPort _leakTester;


        private int _DAQLine; // pin
        private string _DAQPort;


        private Timer _worker;
        private int _workerSpeed;


        internal int _containerSize;
        internal int _piecesOnContainer;
        internal string _currentContainer;
       

        private StringBuilder _leakTesterBuffer;

        internal event EventHandler onChangeState;
        internal event NewPiece OnNewPiece;
        internal event NewActivePartNumber OnNewActivePartNumber;
        internal event ClosingContainer OnClosingContainer;
        internal event RaiseError OnRaiseError;

        internal string _currentPartNumber;
        internal string _pendingRequestPiece;
        private string _lastScannerCodeRead;
        private bool _IsReaderEnabled;

        private Regex _rex;
        


        public LeakTester(string port, int baudRate, int workerSpeed, int DAQLine, string DAQPort)
        {
            this._leakTesterPort = port;
            this._leakTesterBaudRate = baudRate;

            this._workerSpeed = workerSpeed;

            this._DAQLine = DAQLine;
            this._DAQPort = DAQPort;

            this._containerSize = 10;

            this._leakTesterBuffer = new StringBuilder();

            this.pieces = new List<Piece>();
            
        }


        public  void init()
        {
            this._leakTester = new SerialPort(_leakTesterPort, _leakTesterBaudRate);
            this._leakTester.DataReceived += _leakTester_DataReceived;

            this._worker = new Timer(_workerSpeed);
            this._worker.Elapsed += _worker_Elapsed;

            /*
             * Siempre se inicia esperando la prueba de fuga porque no hay trigger.
             * de pieza presente con una señal fisica ni bloqueo.
             */
            this.CurrentStatus = status.WAITING_FOR_PF;

            this.onChangeState(this, null);
            // Monitoring status of signal

            _currentPartNumber = "";

            _rex = new Regex(ConfigurationManager.Instance["LEAK_TESTER_regex"]);

            _IsReaderEnabled = true;
            
        }

        internal void disableReaderResponse()
        {
            _IsReaderEnabled = false;
        }

        public void verifyActivePartNumber()
        {
            string[] msg = new string[4];
            int rest = DAO.GetInstance().numParteActual(ConfigurationManager.Instance["GENERAL_estacion"], ref msg);


           if(rest == 0)
            {
                if(_currentPartNumber != msg[0])
                {
                    _currentPartNumber = msg[0];
                    _containerSize = int.Parse(msg[1]);
                    OnNewActivePartNumber();
                }
            }
        }

        public void proccesScaennerRead(string scannerCode)
        {
            logger.Info("Scanner Code: {0}", scannerCode);
            this.CurrentStatus = status.WAITING_FOR_PF;

            if(_lastScannerCodeRead != scannerCode)
            {
                if (_pendingRequestPiece != "" && _pendingRequestPiece != null)
                {
                    string[] msgs = new string[2];

                    if(DAO.GetInstance().validaEtiqueta(scannerCode.Trim(), _currentPartNumber, ref msgs) == 0)
                    {
                        if (msgs[0].ToUpper() == "MASTER") // no registra, solo pone como maste la pieza
                        {
                            this.pieces.Last<Piece>().sequence = "N/A";
                            this.pieces.Last<Piece>().status = "MASTER";
                        }
                        else
                        {
                            
                            string[] msgs1 = new string[9];
                            if(DAO.GetInstance().registraPieza(_pendingRequestPiece, ConfigurationManager.Instance["GENERAL_estacion"], _currentPartNumber, scannerCode, ref msgs1) == 0)
                            {
                                logger.Info("Prueba de fuga registrada con exito");
                                try
                                {
                                    this._piecesOnContainer = int.Parse(msgs1[4]);
                                    this.pieces.Last<Piece>().sequence = scannerCode;
                                    this.pieces.Last<Piece>().status = "OK";
                                    this._currentContainer = msgs1[3];

                                }catch(Exception e)
                                {
                                    logger.Error(e);
                                }

                            }
                        }
                            
                        OnNewPiece();
                    }
                    else
                    {
                        View.CustomMessageBox.show("ERROR", msgs[0], View.CustomMessageBox._ERROR);
                    }
                    
                }
                else
                {
                    // escaneo invalido, se leera una vez exista pf
                    OnRaiseError("Codigo no valido en este momento!");
                }
            }
            else
            {
                // Codigo invalido
            }

            this.onChangeState(this, null);
        }

        public void triggerToLeakTester(string codeWasRead)
        {
            this.CurrentStatus = status.TRIGGER_PF;
            // Start cycle on leak tester
            IOService.GetInstance().getDevice(0).Ports[_DAQPort][_DAQLine].release();


            this.CurrentStatus = status.WAITING_FOR_PF;

            onChangeState(this, null);
        }

        private void _leakTester_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {

            if (CurrentStatus == status.WAITING_FOR_PF)
            {
                _leakTesterBuffer.Append(_leakTester.ReadExisting());

                logger.Info("Leak Tester result: {0}", _leakTesterBuffer.ToString());

                Match m = _rex.Match(_leakTesterBuffer.ToString());
                if (m.Success)
                {
                    logger.Info("Buffer of Leak Tester completed, begin to proccess data.");
                    if (proccessLeakTesterResult(m.Value))
                    {
                        this.CurrentStatus = status.REGISTERING_PART;
                        _pendingRequestPiece = registerPart(m.Value);

                        this.OnNewPiece();

                        this.CurrentStatus = status.WAITING_FOR_SCANNER;
                        onChangeState(this, null);

                        if(ConfigurationManager.Instance["KEYENCE_READER_enable"].ToLower().Trim() == "false")
                        {
                            string _scannerCode = string.Format("{0:yy}{1:D3}{2:D2}{3:D2}{4:D2}", DateTime.Now, DateTime.Now.DayOfYear, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

                            proccesScaennerRead(_scannerCode);
                        }
                    }
                    else
                    {
                        registerBadPart(m.Value);
                        this.OnNewPiece();
                    }
                    _leakTesterBuffer.Clear();
                }
            }
        }

        public void closeCointainer()
        {
            string[] messages = new string[4];
            DAO.GetInstance().closeContainer(ref messages);

            if (messages[0] == "success" || true) // change it for other stored procedure!
            {
                this._piecesOnContainer = 0;

                OnClosingContainer();

            }
        }

       

        private string registerPart(string pfData)
        {
            Piece currentPiece = new Piece();

            currentPiece.date = DateTime.Now;

            currentPiece.part = _currentPartNumber;
            currentPiece.pfResult= "OK";
            currentPiece.pfString = pfData;

            currentPiece.sequence = "TBD" ;

            currentPiece.status = "PENDING";

            this.pieces.Insert(0, currentPiece);

            return pfData;
        }

        private int registerBadPart(string pfData)
        {
            // Here we go adding new pieces

            string[] msg = new string[3];

            if(DAO.GetInstance().registerBadPiece(pfData, ref msg) >= 0)
            {
                Piece currentPiece = new Piece();

                currentPiece.date = DateTime.Now;

                currentPiece.part = _currentPartNumber;
                currentPiece.pfResult = pfData.Substring(9,6);

                currentPiece.pfString = pfData;

                currentPiece.sequence = "-";

                currentPiece.status = "NOK";

                this.pieces.Insert(0, currentPiece);

            }else
            {
                OnRaiseError(msg[0]);
            }

            return _piecesOnContainer;
        }


        private bool proccessLeakTesterResult(string data)
        {
            // procesar la cadena, y ver si esta bien o mal
            /*
             * 1:Lo No Go 2:GOOD 4:Hi No Go C:HH No Go D:ERROR
             */
            return data[7] == '2';
        }


        private string _currentPFData;
        int c;
        private void _worker_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (c++ < 3) 
            verifyActivePartNumber();
        }

        public void start()
        {
            try
            {
                this._leakTester.Open();
            }
            catch
            {
                throw new Exception("No fue posible establecer comunicación con la prueba de fuga");
            }

            try
            {
                this._worker.Start();
            }
            catch
            {
                throw new Exception("Error al inicializar el worker");
            }
        }

        public void stop()
        {
            this._leakTester.Close();

            this._worker.Stop();
        }
    }
}

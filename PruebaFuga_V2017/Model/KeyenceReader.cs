﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Timers;

namespace PruebaFuga_V2017.Model
{
    class KeyenceReader
    {

        #region < LOGGER >

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion


        private string _keyenceReaderPort;
        private int _keyenceReaderBaudRate;

        private StringBuilder _buffer;

        private SerialPort _keyenceReader;

        private Timer _timeout;

        internal event EventHandler<KeyenceReaderEventArgs> CodeRead;

        public KeyenceReader(string port, int baud)
        {
            this._keyenceReaderPort = port;
            this._keyenceReaderBaudRate = baud;

            _buffer = new StringBuilder();

            _keyenceReader = new SerialPort(this._keyenceReaderPort, this._keyenceReaderBaudRate);

            _keyenceReader.DataReceived += _keyenceReader_DataReceived;

            _timeout = new Timer();
            _timeout.Interval = 500;
            _timeout.Elapsed += _timeout_Elapsed;
        }

        private void _timeout_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timeout.Stop();

            KeyenceReaderEventArgs args = new KeyenceReaderEventArgs();

            args.code = _buffer.ToString();

            OnCodeRead(args);


            _buffer.Clear();

            
        }

        public void start()
        {
            this._keyenceReader.Open();
        }

        private void _keyenceReader_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string _t = _keyenceReader.ReadExisting();
            _buffer.Append(_t);
            logger.Info("Escaner: {0}", _t);



            _timeout.Stop();
            _timeout.Start();

        }

        protected virtual void OnCodeRead(KeyenceReaderEventArgs k)
        {
            EventHandler<KeyenceReaderEventArgs> handler = CodeRead;

            if(handler != null)
            {
                handler(this, k);
            }
        }


    }

    internal class KeyenceReaderEventArgs: EventArgs
    {
        
        internal string code { get; set; }
    }
}

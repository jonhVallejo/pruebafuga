# Marcador para Prueba de Fuga COSMO

## Poka Yoke realizado para Walker Tenneco en 2017

Este pequeño marcador realiza la traza de piezas que pasan por una prueba de fuga, en la cual se debe de capturar los resultados de la pieza e indicar si es pieza mala o pieza buena y bloquear la maquina por medio de **IOs digitales** en caso de que haya algun problema.

Este proyecto hace uso de bibliotecas proporcionadas por **National Instruments y Arduino** así como la comunicación con un lector **DMC Keyence SR2000**, de forma en que el usuario pueda cambiar entre ambos dispositivos de adquisicióm, facilitando el mantenimiento.

De igual forma se trabajo en la interfaz grafica de forma que la aplicación sea mas amigable y diferencie de las clasicas aplicaciones industriales en HMIs de PLC.


 El stack 

- C# .NET
- Oracle 10g
- WPF
- National Instruments 6009
- Arduino Uno
- Keyence Reader SR-2000

## Configuracion

El marcador es configurable mediante un archivo .ini, este tipo de archivos son mas faciles de leer y personal de mantenimiento comumente estan mas familiarizados que con un XML

```INI
[GENERAL]
	name = "Prueba de fuga"
	server = "manager"
	estacion = "EDA"
	pattern = "OK"

[DATA_BASE]
	server = "webagu"
	port = 1521
	user = "WAGUCONV"
	password = "manager"
	sid = "ora10"

[DAQ]
	port = "COM6"
	in_line = 0
	in_port = "port0"
	out_line = 1
	out_port = "port1"
	speed = 2000

[LEAK_TESTER]
	port = "COM3"
	baud = 9600
	regex = "#(0{2} ){2}\w (\+|\-)\d{3,4}.\d? \w \d:\w{2}"

[KEYENCE_READER]
	enable = "false"
	port = "COM13"
	baud = 9600

[SECURITY]
	password = "1234"

```





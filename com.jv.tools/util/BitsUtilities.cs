﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.jv.tools.util
{
    public class BitsUtilities
    {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bits"></param>
        /// <returns></returns>
        public static byte convertBooltArrayToByte(bool[] bits)
        {
            int value = 0;
            for (byte i = 0; i < 7; i++)
            {
                if (bits[i]) value |= 1 << (7 - i);
            }
            return (byte)value;
        }

        public static short convertHexaStringToShort(string hexaString)
        {

            return short.Parse(hexaString, System.Globalization.NumberStyles.HexNumber);
        }

        public static bool[] convertHexaStringToBoolArray(string hexaString)
        {
            bool[] result = new bool[16];

            short number = short.Parse(hexaString, System.Globalization.NumberStyles.HexNumber);

            for (int i = 0; i < 16; i++)
            {
                result[i] = ((number >> i) & 1) != 0;
            }

            return result;
        }

        public static bool[] convertByteToBoolArray(byte value)
        {
            bool[] result = new bool[8];

            for (int i = 0; i < 8; i++)
            {
                result[i] = ((value >> i) & 1) != 0;
            }

            return result; ;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="bits"></param>
        /// <returns></returns>
        public static string convertBooltArrayToString(bool[] bits)
        {
            /*
             * | 15 | 14 | 13 | 12 | 11 | 10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
             * |                                                                     |
             */
            short number = 0;
            for (int i = 15, j = 0; i >= 0; i--, j++)
            {
                if (bits[i])
                    number += (short)Math.Pow(2, j);
            }


            return number.ToString("X4");
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace com.jv.tools.util
{
    public class Collections
    {

        #region < UTILS >

        /// <summary>
        /// Clone objets of the dictionary.
        /// </summary>
        /// <typeparam name="K"></typeparam>
        /// <typeparam name="V"></typeparam>
        /// <param name="dict"></param>
        /// <returns></returns>
        public static Dictionary<K, V> CloneDictionary<K, V>(Dictionary<K, V> dict)
        {
            Dictionary<K, V> newDict = null;

            // The clone method is immune to the source dictionary being null.
            if (dict != null)
            {
                // If the key and value are value types, clone without serialization.
                if (((typeof(K).IsValueType || typeof(K) == typeof(string)) &&
                     (typeof(V).IsValueType) || typeof(V) == typeof(string)))
                {
                    newDict = new Dictionary<K, V>();
                    // Clone by copying the value types.
                    foreach (KeyValuePair<K, V> kvp in dict)
                    {
                        newDict[kvp.Key] = kvp.Value;
                    }
                }
                else
                {
                    // Clone by serializing to a memory stream, then deserializing.
                    // Don't use this method if you've got a large objects, as the
                    // BinaryFormatter produces bloat, bloat, and more bloat.
                    BinaryFormatter bf = new BinaryFormatter();
                    MemoryStream ms = new MemoryStream();
                    bf.Serialize(ms, dict);
                    ms.Position = 0;
                    newDict = (Dictionary<K, V>)bf.Deserialize(ms);
                }
            }

            return newDict;
        }
        #endregion
    }
}

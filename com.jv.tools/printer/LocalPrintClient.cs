﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.jv.tools.printer
{
    class LocalPrintClient : IPrintClient
    {

     


        private string idPrinter;

        public void initPrinter(string printerName)
        {
            this.idPrinter = printerName;
        }

        public bool print(string printCode)
        {
            return RawPrinterHelper.SendStringToPrinter(this.idPrinter, printCode);
        }

        public bool print(string idPrinter, string printCode)
        {
            return RawPrinterHelper.SendStringToPrinter(idPrinter, printCode);
        }
    }
}

﻿using System;
using System.IO;
using System.Runtime;
using System.Runtime.InteropServices;


namespace com.jv.tools.printer
{
    public class Printer
    {
        private const int GENERIC_WRITE = 0x40000000;
        private const int OPEN_EXISTING = 3;
        private const int FILE_SHARE_WRITE = 0x2;

        private StreamWriter _fileWriter;
        private FileStream _outFile;
        private int _hPort;

        /// <summary> 
        /// Structure for CreateFile. Used only to fill requirement 
        /// </summary> 
        [StructLayout(LayoutKind.Sequential)]
        public struct SECURITY_ATTRIBUTES
        {
            private int nLength;
            private int lpSecurityDescriptor;
            private int bInheritHandle;
        }

        [DllImport("kernel32", EntryPoint = "CloseHandle", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int CloseHandle(int hObject);

        [DllImport("kernel32", EntryPoint = "CreateFileA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]//Define Win32 functions 
        private static extern int CreateFile(string lpFileName, int dwDesiredAccess, int dwShareMode, [MarshalAs(UnmanagedType.Struct)] ref SECURITY_ATTRIBUTES lpSecurityAttributes, int dwCreationDisposition, int dwFlagsAndAttributes, int hTemplateFile);


        /// <summary> 
        /// This function must be called first. Printer path must be a COM Port or a UNC path. 
        /// </summary> 
        public void StartWrite(string printerPath)
        {
            SECURITY_ATTRIBUTES SA = default(SECURITY_ATTRIBUTES);

            IntPtr hPortP = default(IntPtr);
            //Dim retval As Integer 

            //Create connection 
            _hPort = CreateFile(printerPath, Printer.GENERIC_WRITE, Printer.FILE_SHARE_WRITE, ref SA, OPEN_EXISTING, 0, 0);

            //Get unsafe pointer 
            hPortP = new IntPtr(_hPort);
            //convert Integer to IntPtr 

            //Create file stream 
            _outFile = new FileStream(hPortP, FileAccess.Write);

            //Create stream writer 
            _fileWriter = new StreamWriter(_outFile);

        }
        /// <summary> 
        /// This will write a command to the printer. 
        /// </summary> 
        public void Write(string rawLine)
        {
            _fileWriter.WriteLine(rawLine);
        }
        /// <summary> 
        /// This function must be called after writing to the zebra printer. 
        /// </summary> 
        public void EndWrite()
        {
            //Clean up 
            _fileWriter.Flush();
            _fileWriter.Close();
            _outFile.Close();
            CloseHandle(_hPort);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.jv.tools.printer
{
    interface IPrintClient
    {
        void initPrinter(string printerName);

        bool print(string printCode);

        bool print(string idPrinter, string printCode);

    }
}

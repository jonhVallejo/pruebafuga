﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace com.jv.tools.printer
{
    class RemotePrintClient : IPrintClient
    {
        #region < LOGGER >

       // private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        private string ipPrinter;

        private string idPrinter;

        private int sockPrinter;



        public void initPrinter(string printerName)
        {
            // Read configuration Printer

            ipPrinter = "";// IniFile.IniReadValue(IniFile._PATH, "PRINT_SERVER", "ip");

            sockPrinter = 0;//int.Parse(IniFile.IniReadValue(IniFile._PATH, "PRINT_SERVER", "port"));

            idPrinter = printerName;//IniFile.IniReadValue(IniFile._PATH, "PRINT_SERVER", "printer");

        }

        public bool print(string printCode)
        {
            // Send content to printer using a socket

            return print(this.idPrinter, printCode);
        }

        public bool print(string idPrinter, string printCode)
        {
            // Send ZPL to printer using a specified  idPrinter
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(ipPrinter), sockPrinter);


            // Create a TCP/IP  socket.
            Socket sender = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);
            byte[] bytes = new byte[1024 * 1];

            try
            {
                sender.Connect(endPoint);

                string Line = printCode;
                string replaceWith = "";

                printCode = Line.Replace("\r\n", replaceWith).Replace("\n", replaceWith).Replace("\r", replaceWith);

                byte[] msg = Encoding.UTF8.GetBytes(printCode + "<<>>" + idPrinter + "\n");


                // Send the data through the socket.
                int bytesSend = sender.Send(msg);

                Console.WriteLine("Bytes for send: {0}", bytesSend);




                int bytesRec = sender.Receive(bytes);


                Console.WriteLine("Result: {0}", Encoding.ASCII.GetString(bytes, 0, bytesRec));


               // logger.Info(Encoding.ASCII.GetString(bytes, 0, bytesRec));
            }
            catch (Exception ex)
            {
             //   logger.Error(ex, "Error al conectar con el servidor");
            }

            return true;
        }


    }
}
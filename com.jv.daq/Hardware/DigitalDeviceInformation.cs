﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.jv.daq.Hardware
{
    public class DigitalDeviceInformation
    {

        public enum Model
        {
            NI6509,
            S2410,
            NI6501,
            ARDUNO,
            TEST
        }

        /// <summary>
        /// Name of the device
        /// </summary>
        public string _deviceName
        {
            get;
            set;
        }

        public string _ip
        {
            get;
            set;
        }

        public int _port
        {
            get;
            set;
        }

        public bool _alive
        {
            get;
            set;
        }

        /// <summary>
        /// Guarda los estados de los puertos, es decir si son de entrada o salida
        /// </summary>
        public Dictionary<string, DigitalDevicePort.TYPE> _configurationPorts
        {
            get;
            set;
        }

        public Model _type
        {
            get;
            set;
        }

        /// <summary>
        /// Number of Input/ Outputs in the device.
        /// </summary>
        public int _size
        {
            get;
            set;
        }

        public DigitalDeviceInformation()
        {
        }

        public DigitalDeviceInformation(string name, Model type)
        {
            this._deviceName = name;
            this._alive = false;
            this._type = type;


            switch (type)
            {
                case Model.NI6509:
                    this._size = 98;
                    break;
                case Model.S2410:
                    this._port = 23;
                    this._size = 48;
                    break;
                case Model.NI6501:
                    this._size = 24;
                    break;
                case Model.TEST:
                    this._size = 24;
                    break;

                case Model.ARDUNO:
                    this._size = 13;
                    break;
            }

            this._configurationPorts = new Dictionary<string, DigitalDevicePort.TYPE>();

            if (type == Model.S2410)
            {
                this._configurationPorts.Add("port0", DigitalDevicePort.TYPE.input);
                this._configurationPorts.Add("port1", DigitalDevicePort.TYPE.input);
                this._configurationPorts.Add("port2", DigitalDevicePort.TYPE.input);
                this._configurationPorts.Add("port3", DigitalDevicePort.TYPE.input);
                this._configurationPorts.Add("port4", DigitalDevicePort.TYPE.input);
                this._configurationPorts.Add("port5", DigitalDevicePort.TYPE.input);
            }


            if (type == Model.ARDUNO)
            {
                this._configurationPorts.Add("port0", DigitalDevicePort.TYPE.input);
                this._configurationPorts.Add("port1", DigitalDevicePort.TYPE.output);
            }

        }

        public DigitalDeviceInformation(string ip, int port, Model type)
        {
            this._deviceName = ip;
            this._alive = false;
            this._type = type;


            switch (type)
            {
                case Model.S2410:
                    this._port = port;
                    this._size = 48;
                    break;
            }

            this._configurationPorts = new Dictionary<string, DigitalDevicePort.TYPE>();

            if (type == Model.S2410)
            {
                this._configurationPorts.Add("port0", DigitalDevicePort.TYPE.input);
                this._configurationPorts.Add("port1", DigitalDevicePort.TYPE.input);
                this._configurationPorts.Add("port2", DigitalDevicePort.TYPE.input);
                this._configurationPorts.Add("port3", DigitalDevicePort.TYPE.input);
                this._configurationPorts.Add("port4", DigitalDevicePort.TYPE.input);
                this._configurationPorts.Add("port5", DigitalDevicePort.TYPE.input);
            }
        }




    }
}

﻿using com.jv.daq.Hardware.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace com.jv.daq.Hardware
{
    public class IOService : I_IOController
    {
        #region < LOGGER >

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion
        /// <summary>
        /// Instancia del singleton, para poder acceder desde cualquier parte.
        /// </summary>
        private static IOService _singletonInstance;

        /// <summary>
        /// // COntiene todos los dispositivos que estan habilitados
        /// </summary>
        private DigitalDeviceContainer devices;

        /// <summary>
        /// Es el hilo que se encarga de verificar la lectura de los dispositovos
        /// </summary>
        private Timer checker;

        /// <summary>
        /// Velocidad con la que se hace la lectura.
        /// </summary>
        private int _speed;

        public int Speed
        {
            get
            {
                return this._speed;
            }
            set
            {
                this._speed = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private List<Tuple<string, bool>> devicesStatus;

        /*
        /// <summary>
        /// Instancia el IO Service, es privado porque solo es utlizado por el singleton.
        /// </summary>
        /// <param name="devices"></param>
        /// <param name="speed"></param>
        private IOService(IDigitalDeviceContainer devices, int speed)
        {
            this.speed = speed;
            this.checker = new Timer();
            this.checker.Interval = speed;
            this.checker.Elapsed += new ElapsedEventHandler(Checker_Elapsed); // += new EventHandler(checker_Tick);
           // this.checker.Elapsed += new ElapsedEventHandler(Checker_Elapsed);
        }
        */
        private void Checker_Elapsed(object sender, ElapsedEventArgs e)
        {
        //    throw new NotImplementedException();
        //}

        //void checker_Tick(object sender, EventArgs e)
       // {
            // Actualizar los dispositvos, hacerles un update. El dispositivo entonces en el update avisa a sus suscriptores de lo que sucede.
#if DEBUG
            logger.Info("Tick de reloj, actualizando datos del service");
#endif



            foreach (IDigitalDevice current in devices)
            {
                // Avisa a todos los dispositivos que se actualicen.
                if (current.isAlive())
                {
                    current.update();
                }
                else
                {
                    // Hacer reconexion
                    try
                    {
                        current.connect();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                    }
                }
            }
        }


        /// <summary>
        /// Instancia el IO Service.
        /// </summary>
        private IOService()
        {
            
        }

        public void init()
        {
            try
            {
                // string sSpeed = IniFile.IniReadValue(IniFile._PATH, "GENERAL", "refresh");
                // if (sSpeed.Length > 0)
                // {
                //   this.speed = Convert.ToInt32(sSpeed);
                // }
                this.checker = new Timer();
                this.checker.Interval = _speed;


                //this.checker.Tick += new EventHandler(checker_Tick);

                this.devices = new DigitalDeviceContainer();

                this.devicesStatus = new List<Tuple<string, bool>>();
            }
            catch (Exception e)
            {
                logger.Error(e, "Sucedio un error mientras se crea el IOService.");
            }
        }

        

        /// <summary>
        /// Agrega un nuevo dispositivo al IOService, el ioservice debe tener concentrados todos los dispositivos y tienen que ser ocupados desde esta clase.
        /// 
        /// </summary>
        /// <param name="device"></param>
        public void putDevice(IDigitalDevice device)
        {
            if (devices == null)
            {
                devices = new DigitalDeviceContainer();
            }
            devices.Add(device);
        }

        public IDigitalDevice getDevice(string name)
        {

            var devR = from IDigitalDevice s in devices where s.deviceInformation._deviceName == name select s;
#if DEBUG
            logger.Info("Dispositivos encontrados: {0}", devR.Count());
            logger.Info("***************************************************************************");
            logger.Info("Nombre: {0}", devR.First().deviceInformation._deviceName);
            foreach (KeyValuePair<string, DigitalDevicePort> currentPort in devR.First().Ports)
            {
                logger.Info("[{0}] = {1}\t{2}", currentPort.Key, currentPort.Value.Type == DigitalDevicePort.TYPE.input ? "INPUT" : "OUTPUT", currentPort.Value.Name);
            }
            logger.Info("***************************************************************************");
#endif
            return devR.Count() > 0 ? devR.First() : null;
        }



        public bool canRead()
        {
            return true;
        }


        public bool canWrite()
        {
            return true;
        }

        public void start()
        {

            logger.Info("Servicio iniciado");

#if DEBUG
            ; //return;
#endif

            Console.WriteLine("NUMERO DE DISPOSITIVOS: {0}", devices.Count);
            foreach (IDigitalDevice currentDevice in devices)
            {
                if (currentDevice.isAlive() == false)
                {
                    try
                    {
                        currentDevice.connect();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error al iniciar IO SERVICE");
                        logger.Error(e);
                    }
                }
            }
            checker.Start();
        }

        public void stop()
        {
            logger.Info("Servicio parado");
            checker.Stop();
        }
        int mutex = 1;

        public IDigitalDevice getDevice(int index)
        {

            if (index >= devices.Count)
            {
                // Tamaño invalido, el indice esta fuera de rango, tiene que arrojar una excepcion.
                throw new Exception("Errror, indice fuera de rango");
            }
            else
            {
                return devices[index];
            }

        }

        public IDigitalDeviceContainer getDevices()
        {
            return devices;
        }

        public static IOService GetInstance()
        {
            if (_singletonInstance == null)
            {
                _singletonInstance = new IOService();
            }
            return _singletonInstance;
        }
    }
}

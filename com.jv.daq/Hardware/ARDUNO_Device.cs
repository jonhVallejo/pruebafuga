﻿using com.jv.daq.Hardware.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ArduinoDriver.SerialProtocol;
using ArduinoUploader;
using NLog;

namespace com.jv.daq.Hardware
{
    public class ARDUNO_Device : IDigitalDevice
    {

        #region < LOGGER >
        /// <summary>
        /// Se obtiene una instancia del logger, es un skeleton.
        /// </summary>
        private static Logger logger = LogManager.GetCurrentClassLogger();
        #endregion


        /// <summary>
        /// Almacena la informacion del dispositivo.
        /// </summary>
        private DigitalDeviceInformation _deviceInformation;




        /// <summary>
        /// Buffer de datos de la tarjeta.
        /// </summary>
        private Dictionary<string, bool[]> dataBuffer;

        /// <summary>
        /// Puertos de la tarjeta.
        /// </summary>
        private Dictionary<string, DigitalDevicePort> ports;

        public enum mode
        {
            NO,
            NA
        };

        public mode _Mode
        {
            get;
            set;
        }



        public Dictionary<string, bool[]> DataBuffer
        {
            get
            {
                return this.dataBuffer;
            }
            set
            {
                this.dataBuffer = value;
            }
        }

        public DigitalDeviceInformation deviceInformation
        {
            get
            {
                return this._deviceInformation;
            }
            set
            {
                this._deviceInformation = value;
            }
        }

        public Dictionary<string, DigitalDevicePort> Ports
        {
            get
            {
                return this.ports;
            }
            set
            {
                this.ports = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return true;
            }
            set
            {
                ;
            }
        }

        public event EventHandler Notify;
        public event onUpdate onUpdateDevice;

        public ARDUNO_Device(string portName)
        {
            deviceInformation = new DigitalDeviceInformation(portName, DigitalDeviceInformation.Model.ARDUNO);

            try
            {
               // mutex = new Semaphore(1, 1);

#if DEBUG
                logger.Info("Creando instancia de tarjeta ARDUINO UNO");
#endif

                dataBuffer = new Dictionary<string, bool[]>();
                bool[] tempBuffer = Enumerable.Repeat(false, 8).ToArray();

                this.dataBuffer.Add("port0", tempBuffer.ToArray());
                this.dataBuffer.Add("port1", tempBuffer.ToArray());


                if (ports == null)
                {
                    this.ports = new Dictionary<string, DigitalDevicePort>();
                    this.ports.Add("port0", new DigitalDevicePort(this, "port0", DigitalDevicePort.TYPE.input));
                    this.ports.Add("port1", new DigitalDevicePort(this, "port1", DigitalDevicePort.TYPE.output));
                }



            }
            catch (Exception er)
            {
                logger.Error("Error!!!!", er);
            }

        }

        public bool connect()

        {
            try
            {
                using (var arduino = new ArduinoDriver.ArduinoDriver(ArduinoUploader.Hardware.ArduinoModel.UnoR3, deviceInformation._deviceName, true))
                {
                    DigitalReadRequest r;

                    DigitalReadResponse res = arduino.Send(new DigitalReadRequest(0)); 
                    
                
               
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool isAlive()
        {
            try
            {
                using (var arduino = new ArduinoDriver.ArduinoDriver(ArduinoUploader.Hardware.ArduinoModel.UnoR3, deviceInformation._deviceName, true))
                {
                    DigitalReadRequest r;

                    DigitalReadResponse res = arduino.Send(new DigitalReadRequest(0));

                    int[] v = new int[100];
                    

                    return true;
                }
            }catch (Exception e)
            {
                return false;
            }
        }

        public bool[] read()
        {
            LinkedList<Object> a;

            bool[] v;

            var filter = _deviceInformation._configurationPorts.Where((e) => e.Value == DigitalDevicePort.TYPE.input);
            v = new bool[(int)filter.Count() * 8];

            using (var arduino = new ArduinoDriver.ArduinoDriver(ArduinoUploader.Hardware.ArduinoModel.UnoR3, deviceInformation._deviceName, true))
            {
                foreach (var item in filter)
                {
                    if (item.Key == "port0")
                    {
                        for (byte i = 0; i < 8; i++)
                        {
                            DigitalReadResponse res = arduino.Send(new DigitalReadRequest(i));
                            v[i] = (res.PinValue == ArduinoDriver.DigitalValue.High);
                            dataBuffer["port0"][i] = v[i];
                        }
                    }
                    else
                    {
                        for (byte i = 8; i < 13; i++)
                        {
                            DigitalReadResponse res = arduino.Send(new DigitalReadRequest(i));
                            v[i] = (res.PinValue == ArduinoDriver.DigitalValue.High);
                            dataBuffer["port1"][i] = v[i - 8];
                        }
                    }
                }
            }
            return v;
        }

        public bool[] read(int beginIndex, int endIndex, string portName)
        {
            return new bool[1];

            //throw new NotImplementedException();
        }

        public bool[] read(int beginIndex, string portName)
        {
            return new bool[1];
           // throw new NotImplementedException();
        }

        public bool readSingleIndex(int index, string portname)
        {
            return true;
            // throw new NotImplementedException();
        }

        public void setInput(string portName)
        {
           // throw new NotImplementedException();
        }

        public void setOutput(string portName)
        {
           // throw new NotImplementedException();
        }

        public void update()
        {
            this.read();

            if (Notify != null)
            {
                Notify(this, new EventArgs());
            }
        }

        public bool write(bool data, int index, string portName)
        {
            using (var arduino = new ArduinoDriver.ArduinoDriver(ArduinoUploader.Hardware.ArduinoModel.UnoR3, deviceInformation._deviceName, true))
            {
                if (this._deviceInformation._configurationPorts[portName] == DigitalDevicePort.TYPE.output)
                {
                    logger.Info("Writing to Arduno device.");
                    int offset = portName == "port0" ? 0 : 8;
                    var sendData = data ? ArduinoDriver.DigitalValue.High : ArduinoDriver.DigitalValue.Low;
                    ArduinoDriver.SerialProtocol.DigitalWriteReponse res = arduino.Send(new DigitalWriteRequest( Convert.ToByte(offset + index), sendData));
                    return res.PinWritten == 1;
                }
                else
                {
                    throw new Exception("El puerto es de lectura no de escritura.");
                }
            }
        }

            public bool write(bool[] data, int beginIndex, string portName)
        {
            return true;
           // throw new NotImplementedException();
        }

        public bool write(bool[] data, int beginIndex, int endIndex, string portName)
        {
            return true;
            // throw new NotImplementedException();
        }

        public bool write(bool[] data, string portName)
        {
            return true;
            // throw new NotImplementedException();
        }
    }
}

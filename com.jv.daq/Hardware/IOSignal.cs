﻿using com.jv.daq.Hardware.Events;
using com.jv.daq.Hardware.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.jv.daq.Hardware
{
    public class IOSignal : I_IOSignal
    {
        #region < LOGGER >

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        #region < DATA MEMBERS >

        public const int _SIGNAL_IN = 0;

        public const int _SIGNAL_OUT = 1;

        public event signalChange _SignalChange;

        public event switchSignal _SignalSwitch;

        private int type;

        private int index;

        private string _portOwner_name;

        private bool state;

        private string _deviceOwner_name;

        #endregion

        #region < ACCESS METHODS >
        public string DeviceOwner_name
        {
            get { return this._deviceOwner_name; }
            set { this._deviceOwner_name = value; }
        }

        public string PortOwner_Name
        {
            get
            {
                return this._portOwner_name;
            }
            set
            {
                this._portOwner_name = value;
            }
        }

        public int Index
        {
            get
            {
                return this.index;
            }
            set
            {
                this.index = value;
            }
        }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="device"></param>
        /// <param name="index"></param>
        /// <param name="type"></param>
        public IOSignal(int index, int type)
        {
            // Check that the index is not out of range in the device.

            this.index = index;
            // Check if the device signal is in or out
            if (type == _SIGNAL_IN || type == _SIGNAL_OUT)
            {
                this.type = type;
            }
            else
            {
                throw new Exception("The type of the signal is undefined.");
            }
        }

        public void doIn()
        {
            this.type = _SIGNAL_IN;
            _SignalSwitch();
        }

        public void doOut()
        {
            this.type = _SIGNAL_OUT;
            _SignalSwitch();

        }

        public void release()
        {
            if (this.type == _SIGNAL_OUT)
            {
                // Avisar con un evento que hace un release
                _SignalChange(this, new SignalEventArgs(true, this.index));
            }
            else // Es una señal IN
            {
                // Avisar al suscriptor de la señal que sucedio el cambio.
                SignalEventArgs evt = new SignalEventArgs(true, this.index);
                _SignalChange(this, evt);
            }
        }

        public void down()
        {
            if (this.type == _SIGNAL_OUT)
            {
                // Escribir al
                _SignalChange(this, new SignalEventArgs(false, this.index));


            }
            else // Es una señal de tipo entrada.
            {
                // Avisar a los suscriptores que se levanto la señal.
                SignalEventArgs evt = new SignalEventArgs(false, this.index);
                _SignalChange(this, evt);
            }
        }

        public override string ToString()
        {
            return "" + index;
        }



    }
}

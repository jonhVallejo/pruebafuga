﻿using com.jv.daq.Hardware.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.jv.daq.Hardware
{
    public class DigitalDeviceContainer : IDigitalDeviceContainer
    {
        private List<IDigitalDevice> devices;

        public DigitalDeviceContainer()
        {
            devices = new List<IDigitalDevice>();
        }


        public DigitalDeviceContainer(IDigitalDevice[] _devices)
        {
            devices = new List<IDigitalDevice>();
            foreach (IDigitalDevice current in _devices)
            {
                this.Add(current);
            }
        }


        public DigitalDeviceContainer(List<IDigitalDevice> _devices)
        {
            devices = new List<IDigitalDevice>();
            foreach (IDigitalDevice current in _devices)
            {
                this.Add(current);
            }
        }

        public DigitalDeviceContainer(IDigitalDevice device)
        {
            devices = new List<IDigitalDevice>();
            devices.Add(device);
        }

        public int IndexOf(IDigitalDevice item)
        {
            return devices.IndexOf(item);
        }

        public void Insert(int index, IDigitalDevice item)
        {
            devices.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            devices.RemoveAt(index);
        }

        public IDigitalDevice this[int index]
        {
            get
            {
                int i = 0;
                IDigitalDevice c = null;
                //do
                //{

                //c = devices.GetEnumerator().Current;
                //} while (i++ <= index && devices.GetEnumerator().MoveNext());
                c = devices[index];
                return c;
            }
            set
            {

            }
        }

        public void Add(IDigitalDevice item)
        {
            devices.Add(item);
        }

        public void Clear()
        {
            devices.Clear();
        }

        public bool Contains(IDigitalDevice item)
        {
            return devices.Contains(item);
        }

        public void CopyTo(IDigitalDevice[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get
            {
                return devices.Count;
            }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(IDigitalDevice item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<IDigitalDevice> GetEnumerator()
        {
            return devices.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
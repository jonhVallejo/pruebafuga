﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.jv.daq.Hardware.Interface
{
    public delegate void onUpdate(Object sender, string portNane, bool[] data);

    /// <summary>
    /// 
    /// </summary>
    public interface IDigitalDevice
    {
        /// <summary>
        /// Notify to suscriptors when a change in the buffer has been suceded.
        /// </summary>
        event EventHandler Notify;

        /// <summary>
        /// Save the current estate into a Array of boolean values from the devices.
        /// </summary>
        Dictionary<string, bool[]> DataBuffer
        {
            get;
            set;
        }

        /// <summary>
        /// Contiene toda la informacion del dispositovo.
        /// </summary>
        DigitalDeviceInformation deviceInformation
        {           //
            get;    // 
            set;    // 
        }           //

        Dictionary<string, DigitalDevicePort> Ports
        {
            get;
            set;
        }

        bool Enabled
        {
            get;
            set;
        }

        event onUpdate onUpdateDevice;


        /// <summary>
        /// Read from all IO's into device.
        /// </summary>
        /// <returns>bool[] array</returns>
        bool[] read();

        /// <summary>
        /// Read only from a especific index into device.
        /// </summary>
        /// <param name="index">Index from read</param>
        /// <returns>bool value</returns>
        bool readSingleIndex(int index, string portname);

        /// <summary>
        /// Read a range into device.
        /// </summary>
        /// <param name="beginIndex"></param>
        /// <param name="endIndex"></param>
        /// <returns></returns>
        bool[] read(int beginIndex, int endIndex, string portName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="beginIndex"></param>
        /// <returns></returns>
        bool[] read(int beginIndex, string portName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        bool write(bool data, int index, string portName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="beginIndex"></param>
        /// <returns></returns>
        bool write(bool[] data, int beginIndex, string portName);

        /// <summary>
        ///  
        /// </summary>
        /// <param name="data"></param>
        /// <param name="beginIndex"></param>
        /// <param name="endIndex"></param>
        /// <returns></returns>
        bool write(bool[] data, int beginIndex, int endIndex, string portName);

        /// <summary>
        /// 
        /// </summary>C:\Users\Jonathan\Desktop\jona_memory_backup_20_oct\AndonMonitor\AndonMonitor\Images\
        /// <param name="data"></param>
        /// <returns></returns>
        bool write(bool[] data, string portName);

        /// <summary>
        /// Check if the device is active.
        /// </summary>
        /// <returns>bool value</returns>
        bool isAlive();

        /// <summary>
        /// Coonect to device.
        /// </summary>
        /// <returns>bool value</returns>
        bool connect();

        /// <summary>
        /// Update the device and call to eventHandler for notify to suscriptors.
        /// </summary>
        void update();


        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        void setInput(string portName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        void setOutput(string portName);



    }
}

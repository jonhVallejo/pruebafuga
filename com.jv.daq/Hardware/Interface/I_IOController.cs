﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.jv.daq.Hardware.Interface
{
    public interface I_IOController
    {
        // Representa una abstraccion de un conjunto de entradas y salidas de la tarjeta, se encarga de decidir a quien y de quien leer cuando se tienen mas
        // de una tarjeta de entradas y salidas instanciada.


        #region < UTILITIES METHODS >

        bool canRead();
        bool canWrite();

        #endregion


        /// <summary>
        /// 
        /// </summary>
        void start();

        /// <summary>
        /// 
        /// </summary>
        void stop();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        IDigitalDevice getDevice(int index);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IDigitalDeviceContainer getDevices();
    }
}

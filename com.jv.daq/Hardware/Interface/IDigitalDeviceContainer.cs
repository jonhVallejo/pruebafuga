﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.jv.daq.Hardware.Interface
{
    /// <summary>
    /// Representa una abstraccion de un contenedor de tarjetas.
    /// Este almacenera multiples tarjetas de entradas y salidas.
    /// De forma que se pueda hacer hibrida la conbinacion de tarjetas.
    /// </summary>
    public interface IDigitalDeviceContainer : IList<IDigitalDevice>
    {

    }
}
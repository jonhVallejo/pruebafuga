﻿using com.jv.daq.Hardware.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.jv.daq.Hardware.Interface
{
    public delegate void signalChange(Object owner, SignalEventArgs args);
    public delegate void switchSignal();
    /// <summary>
    /// Autor: Ing. Francisco Javier Castelan, Jonathan Vallejo
    /// Date: Feb 2014
    /// Propourse:
    ///     Representa una abstracción de una señal digital, la cual puede cambiar de señal de entrada a señal de salida. Ademas de que cuando es señal de entrada, puede avisar de su estado a quien la utilice.
    /// </summary>
    public interface I_IOSignal
    {
        /// <summary>
        /// 
        /// </summary>

        event signalChange _SignalChange;

        event switchSignal _SignalSwitch;

        string DeviceOwner_name
        {
            get;
            set;
        }

        string PortOwner_Name
        {
            get;
            set;
        }

        int Index
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        void doIn();

        /// <summary>
        /// 
        /// </summary>
        void doOut();

        /// <summary>
        /// 
        /// </summary>
        void release();

        /// <summary>
        /// 
        /// </summary>
        void down();


    }



}
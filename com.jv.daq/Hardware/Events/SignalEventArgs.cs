﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.jv.daq.Hardware.Events
{
    public class SignalEventArgs : EventArgs
    {
        /// <summary>
        /// Construye una instancia de SignalEventArgs, contiene información de el valor de cambio en la señal.
        /// </summary>
        /// <param name="v">Valor de la señal</param>
        /// <param name="p">Id de la señak</param>
        public SignalEventArgs(bool v, int p)
        {
            this.value = v;
            this.owner = p;
        }

        /// <summary>
        /// Valor de la señal
        /// </summary>
        private bool value;

        /// <summary>
        /// Id de la señal que lo envia.
        /// </summary>
        private int owner;

        /// <summary>
        /// Metodo de acceso para obtener el valor de la señal
        /// </summary>
        public bool Value
        {
            get
            {
                return this.value;
            }

            set
            {
                this.value = value;
            }
        }

        /// <summary>
        /// Metodo de acceso para obtener el id de la señal.
        /// </summary>
        public int Owner
        {
            get
            {
                return this.owner;
            }

            set
            {
                this.owner = value;
            }
        }
    }
}

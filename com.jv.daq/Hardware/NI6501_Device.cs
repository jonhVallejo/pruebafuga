﻿using com.jv.daq.Hardware.Interface;
using NationalInstruments.DAQmx;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace com.jv.daq.Hardware
{
    public class NI6501_Device : IDigitalDevice
    {

        #region < LOGGER >
        /// <summary>
        /// Se obtiene una instancia del logger, es un skeleton.
        /// </summary>
        private static Logger logger = LogManager.GetCurrentClassLogger();
        #endregion


        /// <summary>
        /// Almacena la informacion del dispositivo.
        /// </summary>
        private DigitalDeviceInformation _deviceInformation;


        public event EventHandler Notify;


        /// <summary>
        /// Buffer de datos de la tarjeta.
        /// </summary>
        private Dictionary<string, bool[]> dataBuffer;

        /// <summary>
        /// Puertos de la tarjeta.
        /// </summary>
        private Dictionary<string, DigitalDevicePort> ports;

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, DIChannel> DIChannels;


        private NationalInstruments.DAQmx.Task digitalTaskIn;


        private DigitalSingleChannelReader _reader;


        /// <summary>
        /// Semaforo para garantizar la exclusion mutua.
        /// </summary>
        private System.Threading.Semaphore mutex;



        public NI6501_Device(string devName)
        {
            this._deviceInformation = new DigitalDeviceInformation(devName, DigitalDeviceInformation.Model.NI6501);

            this.connect();
        }

        public Dictionary<string, bool[]> DataBuffer
        {
            get
            {
                return this.dataBuffer;
            }
            set
            {
                this.dataBuffer = value;
            }
        }

        public DigitalDeviceInformation deviceInformation
        {
            get
            {
                return this._deviceInformation;
            }
            set
            {
                this._deviceInformation = value;
            }
        }

        public Dictionary<string, DigitalDevicePort> Ports
        {
            get
            {
                return this.ports;
            }
            set
            {
                this.ports = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return true;
            }
            set
            {
                ;
            }
        }

        public event onUpdate onUpdateDevice;

        public bool[] read()
        {



            mutex.WaitOne();
            // this.dataBuffer["port0"] = Util.BitsUtilities.convertByteToBoolArray( this._reader.ReadMultiSamplePortByte(1)[0]);
            this.dataBuffer["port1"] = this._reader.ReadSingleSampleMultiLine();

#if DEBUG

            for (int i = 0; i < 8; i++)
            {
                logger.Info("[{0}]:{1} ", i, this.dataBuffer["port1"][i]);
            }
#endif
            // Checar si hay cambios en la señal
            mutex.Release();


            return this.dataBuffer["port1"];
        }





        public bool readSingleIndex(int index, string portname)
        {
            throw new NotImplementedException();
        }

        public bool[] read(int beginIndex, int endIndex, string portName)
        {
            throw new NotImplementedException();
        }

        public bool[] read(int beginIndex, string portName)
        {
            throw new NotImplementedException();
        }

        public bool write(bool data, int index, string portName)
        {
            throw new NotImplementedException();
        }

        public bool write(bool[] data, int beginIndex, string portName)
        {
            throw new NotImplementedException();
        }

        public bool write(bool[] data, int beginIndex, int endIndex, string portName)
        {
            throw new NotImplementedException();
        }

        public bool write(bool[] data, string portName)
        {
            throw new NotImplementedException();
        }

        public bool isAlive()
        {
            return true;
        }

        public bool connect()
        {
            try
            {
                mutex = new Semaphore(1, 1);

#if DEBUG
                logger.Info("Creando instancia de tarjeta NI6509");
#endif

                dataBuffer = new Dictionary<string, bool[]>();
                bool[] tempBuffer = Enumerable.Repeat(false, 8).ToArray();

                this.dataBuffer.Add("port0", tempBuffer.ToArray());
                this.dataBuffer.Add("port1", tempBuffer.ToArray());
                this.dataBuffer.Add("port2", tempBuffer.ToArray());


                if (ports == null)
                {
                    this.ports = new Dictionary<string, DigitalDevicePort>();
                    this.ports.Add("port0", new DigitalDevicePort(this, "port0", DigitalDevicePort.TYPE.input));
                    this.ports.Add("port1", new DigitalDevicePort(this, "port1", DigitalDevicePort.TYPE.input));
                    this.ports.Add("port2", new DigitalDevicePort(this, "port2", DigitalDevicePort.TYPE.input));
                }



            }
            catch (Exception er)
            {
                logger.Error("Error!!!!", er);
            }

            digitalTaskIn = new NationalInstruments.DAQmx.Task();


            DIChannel channelIn;

            this.DIChannels = new Dictionary<string, DIChannel>();

            // Señal de entrada sera la unica por el momento

            channelIn = digitalTaskIn.DIChannels.CreateChannel(String.Format("{0}/port1:1", this.deviceInformation._deviceName), "port1", ChannelLineGrouping.OneChannelForAllLines);
            this.DIChannels.Add("port1", channelIn);

            digitalTaskIn.Control(TaskAction.Verify);

            _reader = new DigitalSingleChannelReader(digitalTaskIn.Stream);




            return false;
        }

        public void update()
        {
            Dictionary<string, bool[]> dt = com.jv.tools.util.Collections.CloneDictionary(dataBuffer);

            this.read();

            if (Notify != null)
            {
                Notify(this, new EventArgs());
            }

            /*
            foreach (KeyValuePair<string, bool[]> currentPortBuffer in dt)
            {
                for (int i = 0; i < 8; i++)
                {
                    if (currentPortBuffer.Value[i] != dataBuffer[currentPortBuffer.Key][i])
                    {
                        Notify(this, new EventArgs());
                        break;
                    }
                }
            }
             * */
        }

        public void setInput(string portName)
        {
            throw new NotImplementedException();
        }

        public void setOutput(string portName)
        {
            throw new NotImplementedException();
        }
    }
}

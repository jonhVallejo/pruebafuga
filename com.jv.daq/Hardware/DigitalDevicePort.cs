﻿using com.jv.daq.Hardware.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.jv.daq.Hardware
{
    public class DigitalDevicePort : List<I_IOSignal>
    {
        #region < LOGGER >

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        #region < CONSTANTS >


        public enum TYPE
        {
            input,
            output
        };

        /// <summary>
        /// Es el tamaño del puerto, no se puede alterar, es predeterminado
        /// </summary>
        public const int _SIZE = 8;

        public const int _IN = 0;
        public const int _OUT = 1;

        #endregion

        #region < DATA MEMBERS >
        /// <summary>
        /// Guarda toda la información del puerto, para saber si hay cambios entre cada notificación
        /// del device.
        /// </summary>
        private bool[] buffer;

        /// <summary>
        /// El dispositivo del que se registran los cambios en la señal.
        /// </summary>
        private IDigitalDevice device;

        /// <summary>
        /// Tipo de puerto, entrada o salida.
        /// Puede tomar los valores:
        ///     0 - entrada
        ///     1 - salida
        /// </summary>
        private TYPE type;

        /// <summary>
        /// Nombre del puerto
        /// </summary>
        private string name;


        /// <summary>
        /// El indice donde empieza el puerto, para que haga las 
        /// busquedas en el device. El cual contiene un buffer
        /// general de todas las IO's.
        /// </summary>
        //private int beginIndex;

        #endregion

        #region < CONSTRUCTOR >
        /// <summary>
        /// Construye una instancia de DigitalDevicePort.
        /// 
        /// </summary>
        /// <param name="device">Dispositivo asociado al puerto.</param>
        public DigitalDevicePort(IDigitalDevice device, string name)
        {

            this.name = name;

            this.device = device;
            this.device.Notify += new EventHandler(device_Notify);

            for (int i = 0; i < _SIZE; i++)
            {
                // Se instancian 
                Add(new IOSignal(i, IOSignal._SIGNAL_IN));
                // Añadir el handler a las señales.

                this[i]._SignalChange += new signalChange(DigitalDevicePort__SignalChange);
            }
            buffer = Enumerable.Repeat(false, 8).ToArray();
        }

        /// <summary>
        /// Construye una instancia de DigitalDevicePort.
        /// 
        /// </summary>
        /// <param name="device">Dispositivo asociado al puerto.</param>
        public DigitalDevicePort(IDigitalDevice device, string name, TYPE type)
        {
            this.type = type;
            this.name = name;

            this.device = device;
            this.device.Notify += new EventHandler(device_Notify);

            for (int i = 0; i < _SIZE; i++)
            {
                // Se instancian 
                Add(new IOSignal(i, type == TYPE.input ? IOSignal._SIGNAL_IN : IOSignal._SIGNAL_OUT));
                // Añadir el handler a las señales.
                this[i]._SignalChange += new signalChange(DigitalDevicePort__SignalChange);
                this[i].DeviceOwner_name = this.device.deviceInformation._deviceName;
                this[i].PortOwner_Name = this.name;
                this[i].Index = i;
            }
            buffer = Enumerable.Repeat(false, 8).ToArray();
        }


        #endregion

        #region < ACCESS METHODS >

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public TYPE Type
        {
            get
            {
                return this.type;
            }
        }

        public IDigitalDevice owner
        {
            get
            {
                return this.device;
            }
        }

        #endregion

        #region < FRIEND METHODS >

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void device_Notify(object sender, EventArgs e)
        {
            // Avisa a todas las señales que se cambio el estado, de echo busca si le corresponde el indice deseado y lo cambia.
            // Contiene un buffer
            if (this.type == TYPE.input)
            {
                // Acceder al buffer del device.
                bool[] tData = device.DataBuffer[this.name];

                for (int i = 0; i < _SIZE; i++)
                {
                    if (tData[i] != buffer[i])
                    {
                        // Avisar a esa señal que sucedio un cambio, bueno hacer la operacion correspondiente
                        if (tData[i])
                        {
                            this[i].release();
                        }
                        else
                        {
                            this[i].down();
                        }
                    }
                }
                Array.Copy(tData, buffer, _SIZE);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="args"></param>
        void DigitalDevicePort__SignalChange(object owner, Events.SignalEventArgs args)
        {
            // La señal nos envia un evento y necesitamos valdiar
            // 1. Es un puerto de entrada o de salida.
            //      - Si es de salida, entonces enviamos la notificación al device.
            // 2. Solo enviar el bit que se necesita escribir, para eso sacar el id del owner, del evento.
            if (this.type == TYPE.output)
            {
                device.write(args.Value, args.Owner, this.name);
            }
        }

        /// <summary>
        /// Cambia el tipo de puerto, es decir: si es de entradas, lo hace de salidas y viceversa.
        /// </summary>
        public void switchPort()
        {
            if (this.type == TYPE.input)
            {


                // Notificar a todos sus iosignal, que ya son señales de salida.
                for (int i = 0; i < this.Count; i++)
                {
                    this[i].doOut();
                }
                device.setOutput(this.name);
                this.type = TYPE.output;
            }
            else
            {


                // Notificar a todos sus iosignal, que ya son señales de salida.
                for (int i = 0; i < this.Count; i++)
                {
                    this[i].doIn();
                }
                device.setInput(this.name);
                this.type = TYPE.input;
            }

        }

        public override string ToString()
        {
            return this.name;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using System.IO.Ports;

namespace com.jv.serialTester
{
    public partial class Form1 : Form
    {

        #region < LOGGER >

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        SerialPort _sp;

        public Form1()
        {
            InitializeComponent();

            this.comboBox1.Items.AddRange( SerialPort.GetPortNames());

            
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.comboBox1.SelectedItem != null && this.comboBox2.SelectedItem != null)
            {
                if(_sp != null)
                {
                    try
                    {
                        _sp.Close();
                    }
                    catch
                    {
                    }
                }
                _sp = new SerialPort();

                _sp.PortName = this.comboBox1.SelectedItem.ToString();
                _sp.BaudRate = int.Parse(this.comboBox2.SelectedItem.ToString());
                _sp.DataReceived += _sp_DataReceived;

                _sp.Open();

                if (!_sp.IsOpen)
                {
                    MessageBox.Show("Imposible abrir puerto, intentar nuevamente o, con otro puerto");
                }

                groupBox1.Enabled = true;
            }
            else
            {
                MessageBox.Show("Debes seleccionar algo");
            }
        }


        StringBuilder _buffer = new StringBuilder();

        const string OK = "SOF", RESET = "SRF", SEF = "SEF";

        private void _sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {


            string text = _sp.ReadExisting();
            textBox2.AppendText(text);
            textBox2.AppendText("\n");

            if (textBox2.TextLength >= 1000)
                textBox2.Clear();

            logger.Info(text);

            
        }

       

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter && false)
            {
                send(textBox1.Text);
                textBox1.Clear();
            }
        }

        private void send(string text)
        {
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep((int)numericUpDown1.Value * 1000);
                _sp.Write(text);
            });
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (true) return;
            send(textBox1.Text);
            textBox1.Clear();
        }
    }
}
